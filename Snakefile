# type: ignore

from hydra import compose, initialize

MODELS = ["LogisticRegression", "DecisionTree", "RandomForest"]
# MODELS = ["DecisionTree"]
# MODELS = ["DecisionTree", "RandomForest"]
DATASETS = ["ny_street_trees"]
PREPROCESSING = ["1", "2"]

initialize(version_base=None, config_path='src/conf')
hydra_cfg = compose(config_name="config")

rule all:  # noqa: E999
    input:
        expand("models/model_{model}_{prepocessing}.pickle", model=MODELS, prepocessing=PREPROCESSING)

rule loading:
    output:
        expand("data/raw/{dataset}.csv", dataset=DATASETS)
    resources: mem_mb=1000
    shell:
        "python src/scripts/step_1_loading.py"

rule preprocessing_1:
    input:
        expand("data/raw/{dataset}.csv", dataset=DATASETS)
    output:
        "data/processed/processed_data_1.csv"
    shell:
        # "python src/scripts/step_2_preprocessing_1.py data/raw/ny_street_trees.csv > data/processed/processed_data_1.csv"
        "python src/scripts/step_2_preprocessing_1.py"

rule preprocessing_2:
    input:
        expand("data/raw/{dataset}.csv", dataset=DATASETS)
    output:
        "data/processed/processed_data_2.csv"
    shell:
        # "python src/scripts/step_2_preprocessing_2.py data/raw/ny_street_trees.csv > data/processed/processed_data_2.csv"
        "python src/scripts/step_2_preprocessing_2.py"

rule modeling_LogisticRegression:
    input:
        expand("data/processed/processed_data_{prepocessing}.csv", prepocessing=PREPROCESSING)
    output:
        expand("models/model_LogisticRegression_{prepocessing}.pickle", prepocessing=PREPROCESSING)
    threads: 2
    shell:
        "python src/scripts/step_3_modeling_LogisticRegression.py"

rule modeling_DecisionTree:
    input:
        expand("data/processed/processed_data_{prepocessing}.csv", prepocessing=PREPROCESSING)
    output:
        expand("models/model_DecisionTree_{prepocessing}.pickle", prepocessing=PREPROCESSING)
    threads: hydra_cfg['cores'] // 2
    shell:
        "python src/scripts/step_3_modeling_DecisionTree.py"

rule modeling_RandomForest:
    input:
        expand("data/processed/processed_data_{prepocessing}.csv", prepocessing=PREPROCESSING)
    output:
        expand("models/model_RandomForest_{prepocessing}.pickle", prepocessing=PREPROCESSING)
    threads: hydra_cfg['cores'] // 2
    shell:
        "python src/scripts/step_3_modeling_RandomForest.py"