import os

import hydra
import pandas as pd
from omegaconf import DictConfig


@hydra.main(version_base=None, config_path="../conf", config_name="config.yaml")
def main(cfg: DictConfig) -> None:
    df = pd.read_csv("data/raw/ny_street_trees.csv")

    # Remove all the rows for trees that are dead or stumps.
    # This should remove many of the nulls in the dataset.
    df = df[df["status"] == "Alive"].copy().reset_index(drop=True)

    # Creating a dictionary for mapping purposes
    health_status = {"Good": 1, "Fair": 2, "Poor": 3}

    # Making a new column called 'health_status'
    df["health_status"] = df["health"].map(health_status)

    df = pd.get_dummies(df, columns=["spc_common"], prefix="name", drop_first=True)

    # Recode a few more columns that I want to use in my model so that the values are no longer strings
    df["curb_loc"] = df["curb_loc"].map({"OnCurb": 1, "OffsetFromCurb": 0})
    df["steward"] = df["steward"].map({"1or2": 1, "3or4": 2, "4orMore": 3, "None": 0})
    df["guards"] = df["guards"].map(
        {"Harmful": 2, "Helpful": 3, "Unsure": 1, "None": 0}
    )
    df["sidewalk"] = df["sidewalk"].map({"Damage": 1, "NoDamage": 0})
    df["root_stone"] = df["root_stone"].map({"Yes": 1, "No": 0})
    df["root_grate"] = df["root_grate"].map({"Yes": 1, "No": 0})
    df["root_other"] = df["root_other"].map({"Yes": 1, "No": 0})
    df["trunk_wire"] = df["trunk_wire"].map({"Yes": 1, "No": 0})
    df["trnk_light"] = df["trnk_light"].map({"Yes": 1, "No": 0})
    df["trnk_other"] = df["trnk_other"].map({"Yes": 1, "No": 0})
    df["brch_light"] = df["brch_light"].map({"Yes": 1, "No": 0})
    df["brch_shoe"] = df["brch_shoe"].map({"Yes": 1, "No": 0})
    df["brch_other"] = df["brch_other"].map({"Yes": 1, "No": 0})

    # Dropping the rows with nulls for 'health_status', 'guards', and 'sidewalk'
    df.drop(df.index[[31282, 407647, 329915]], inplace=True)

    df.dropna(inplace=True)

    # Determine the row index where you want to split the DataFrame
    divide_by = cfg["preprocessing"]["params"]["divide_by"]
    split_index = len(df) // divide_by

    # Get the second half of the DataFrame
    second_half_df = df.iloc[split_index:]

    # Define the directory path for saving files
    processed_dir = "data/processed"

    # Ensure that the directory exists, create it if necessary
    if not os.path.exists(processed_dir):
        os.makedirs(processed_dir)

    # Save the second half to a CSV file
    second_half_df.to_csv(
        os.path.join(processed_dir, "processed_data_2.csv"), index=False
    )


if __name__ == "__main__":
    main()
