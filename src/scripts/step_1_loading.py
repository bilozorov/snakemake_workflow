import os
import time
import zipfile

from kaggle.api.kaggle_api_extended import KaggleApi

# Define the directory path for saving files
data_dir = "data/raw"

# Instantiate the Kaggle API object
api = KaggleApi()

# Set the Kaggle username and API key
api.authenticate()

# Download the dataset
api.dataset_download_files(
    dataset="new-york-city/ny-2015-street-tree-census-tree-data", path=data_dir
)

# Wait for the dataset to be downloaded
downloaded = False
while not downloaded:
    downloaded = os.path.exists(
        os.path.join(data_dir, "ny-2015-street-tree-census-tree-data.zip")
    )
    time.sleep(1)  # Wait for 1 second

# Extract the CSV file from the downloaded zip file
zip_file_path = os.path.join(data_dir, "ny-2015-street-tree-census-tree-data.zip")
with zipfile.ZipFile(zip_file_path, "r") as zip_ref:
    zip_ref.extractall(data_dir)

# Remove the downloaded zip file
os.remove(zip_file_path)
os.remove(os.path.join(data_dir, "socrata_metadata.json"))
os.remove(os.path.join(data_dir, "StreetTreeCensus2015TreesDataDictionary20161102.pdf"))

# Rename the extracted CSV file
old_csv_path = os.path.join(data_dir, "2015-street-tree-census-tree-data.csv")
new_csv_path = os.path.join(data_dir, "ny_street_trees.csv")
os.rename(old_csv_path, new_csv_path)

print("Dataset downloaded, extracted, and processed successfully!")
